<?php

/**
 * Returns path to a Flexi CMS folder.
 *
 * @param  string $section
 * @return string
 */
function path($section)
{
    $pathMask = ROOT_DIR . DS . '%s';
    
    if(ENV == 'Cms')
        $pathMask = ROOT_DIR . DS . 'cms' . DS . '%s';
    // Путь к секции
    switch (strtolower($section))
    {
        case 'controller':
            return sprintf($pathMask,'Controller');
        case 'config':
            return sprintf($pathMask,'Config');
        case 'model':
            return sprintf($pathMask,'Model');
        case 'view':
            return sprintf($pathMask,'View');
        default:
            return ROOT_DIR;
    }
}
