<?php

//Подлючаем autoloader
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/Function.php';

class_alias('Engine\\Core\\Template\\Asset', 'Asset');
class_alias('Engine\\Core\\Template\\Theme', 'Theme');
class_alias('Engine\\Core\\Template\\Menu', 'Menu');

use Engine\Cms;
use Engine\DI\DI;

try {
    /**
     * Экземпляр внедрение зависимости
     */
    $di = new DI();
    
    /**
     * Массив для хранения сервисов
     */
    $services = require __DIR__ . '/Config/Service.php';
    
    /**
     * Проходим по массиву сервисов и инициализируем каждый
     */
    foreach ($services as $service){
        $provider = new $service($di);
        $provider->init();
    }
    /**
     * Экземпляр cms
     */
    $cms = new Cms($di);
    
    /**
     * Запуск приложения
     */
    $cms->run();
  
        
} catch (\Exception $e) {
    echo $e->getMessage();
}

