<?php

namespace Engine\Helper;

class Common {
    /**
     * Функция для получения метода запроса
     * 
     * $return boolean
     */
    public static function getMethod(){
        return $_SERVER['REQUEST_METHOD'];
    }
    
    /**
     * Функция для определения метода POST
     * 
     * @return boolean
     */
    
    public static function isPost() {
        if($_SERVER['REQUEST_METHOD']=='POST')
        {
            return true;
        }
        return false;
    }
    
    /**
     * Фунция для получения URL
     */
    public static function getPathUrl(){
        $pathUrl = $_SERVER['REQUEST_URI'];
        
        /*Проверяем, используется ли метод GET*/
        if($position = strpos($pathUrl, '?'))
        {
            $pathUrl = substr($pathUrl,0,$position);
        }
        
        return $pathUrl;
    }
}
