<?php

namespace Engine\Helper;

class Cookie {
    
    /**
     * Добавляем куки
     * 
     * @param type $key
     * @param type $value
     * @param type $time
     */
    public static function set($key, $value, $time = 31536000)
    {
        setcookie($key,$value, time() + $time,'/');
    }
    
    /**
     * Получаем куки по ключу
     * 
     * @param type $key
     * @return type
     */
    public static function get($key)
    {
        if( isset($_COOKIE[$key]))
        {
            return $_COOKIE[$key];
        }
        return null;
    }
    
    /**
     * Удаляем куки по ключу
     * 
     * @param type $key
     */
    public static function delete($key)
    {
        if( isset($_COOKIE[$key]))
        {
            self::set($key,'',-3600);
            unset($_COOKIE[$key]);
        }
    }
}
