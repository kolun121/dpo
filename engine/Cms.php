<?php
namespace Engine;

use Engine\Helper\Common;
use Cms\Controller\HomeController;
use Engine\Core\Router\Router;

class Cms {
    /**
     *
     * @var Obj 
     */
    private $di;
    
    public $router;
    
    /**
     * 
     * 
     * @param object $di
     */
    public function __construct($di) {
        $this->di=$di;
        $this->router = $this->di->get('router');
    }
    
    /**
     * Функция старта приложения
     */
    public function run(){
        try{
            //$this->di->get('db')->query('SELECT name,date,type_of_block,type_of_course,image FROM catalog');

            //ENV - константа окружения (в index.php и admin/index.php)
            //Добавляем роуты
            require_once __DIR__ . '/../' . mb_strtolower(ENV) . '/Routes.php';
            //require_once __DIR__ . '/../cms/Routes.php';
           
            $routerDispatch = $this->router->dispatch(Common::getMethod(),Common::getPathUrl());
           
            if($routerDispatch==null){
                $routerDispatch=new Core\Router\DispatchedRoute('ErrorController:page404');
            }
            list($class,$action) = explode(':',$routerDispatch->getController(),2);
            $parameters = $routerDispatch->getParameters();
          
            //ENV - константа окружения (в index.php и admin/index.php)
            $controller = '\\' . ENV . '\\Controller\\' . $class;
            call_user_func_array([new $controller($this->di),$action],$routerDispatch->getParameters());
            

        }catch(\Exception $e){
            echo $e->getMessage();
            exit;
        }

    }
}
