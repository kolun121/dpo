<?php

namespace Engine\Core\Template;

use Engine\DI\DI;
use Cms\Model\Menu\MenuRepository;

class Menu 
{
    protected static $di;
    
    protected static $menuRepository;
    
    public function __construct(DI $di) {
        self::$di = $di;
        self::$menuRepository = new MenuRepository($di);
    }
    
    public function show() {
        
    }
    
    public static function getItems(){
        return self::$menuRepository->getAllItems();
    }
}
