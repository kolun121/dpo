<?php

namespace Engine\Core\Template;

use Engine\Core\Template\Theme;

class View {
    public $di;
    
    protected $theme;
    
    protected $menu;
    
    public function __construct($di) {
        $this->di = $di;
        $this->theme = new Theme();
        $this->menu = new Menu($di);
    }
    
    public function render($template,$vars = []){
        $function = Theme::getThemePath() . '/functions.php';
        if(file_exists($function)){
            include_once $function;
        }
        
        $templatePath = $this->getTemplatePath($template, ENV);
        //Если шаблон не найден
        if(!is_file($templatePath)){
            throw new \InvalidArgumentException(sprintf('Шаблон "%s" не найден в "%s"',$template,$templatePath));
            //echo 'Шаблон ' .$templatePath. ' не найден';
        }
        
        //Устанавливаем переданный массив с переменными
        $this->theme->setData($vars);
        //Представляем ключи массива vars как переменные
        extract($vars);
        
        //Создание буфера
        ob_start();
        ob_implicit_flush(0);
        try{
            require $templatePath;
        }catch(\Exception $e){
            ob_end_clean();
            throw $e;
        }
        
        echo ob_get_clean();
    }
    
    /**
     * Получаем путь к шаблонам в зависимости от окружения
     * 
     * @param type $template
     * @param type $env
     * @return type
     */
    private function getTemplatePath($template,$env = null){
        if($env == 'Cms')
        {
            return ROOT_DIR . '/content/themes/default/' . $template . '.php';
        }
        return ROOT_DIR . '/View/' . $template . '.php';
    }
    
    private function getThemePath(){
        return ROOT_DIR . '/content/themes/default';
    }
 
}
