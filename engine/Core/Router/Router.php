<?php
namespace Engine\Core\Router;

class Router {
    /**
     *Списки всех путей
     * 
     * @var array 
     */
    private $routes = [];
    
    private $dispatcher;
    
    /**
     * Название хоста (ссылка)
     * 
     * @var String
     */
    private $host;

    /**
     * Конструктор
     * 
     * @param type $host
     */
    public function __construct($host) {
        $this->host=$host;
    }
    
    /**
     * Функция для добавления путей
     * 
     * @param type $key
     * @param type $pattern
     * @param type $controller
     * @param type $method
     */
    public function add($key,$pattern,$controller,$method='GET') {
        $this->routes[$key]=[
            'pattern'    => $pattern,
            'method'     => $method,
            'controller' => $controller
        ];
    }
    
    public function dispatch($method,$uri) {
        return $this->getDispatcher()->dispatch($method, $uri);
    }
    
    public function getDispatcher(){
        if($this->dispatcher==null)
        {
            $this->dispatcher= new UrlDispatcher();
            
            foreach ($this->routes as $route)
            {
                $this->dispatcher->register($route['method'],$route['pattern'],$route['controller']);
            }
        }
        return $this->dispatcher;
    }
    
}
