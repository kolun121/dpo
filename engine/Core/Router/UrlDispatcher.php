<?php

namespace Engine\Core\Router;

class UrlDispatcher {
    /**
     * Массив для хранения типов метода
     * 
     * @var array 
     */
    private $methods = [
        'GET',
        'POST'
    ];
    
    /**
     * Массив для путей в зависимости от метода обращения
     * 
     * @var array
     */
    private $routes = [
        'GET'  => [],
        'POST' => []
    ];
    
    /**
     * Патерны регулярных выражений (числа, строки, строки + числа)
     * 
     * @var array
     */
    private $patterns = [
        'int' => '[0-9]+',
        'str' => '[a-zA-Z\.\-_%]+',
        'any' => '[a-zA-Z0-9\.\-_%]+'
    ];
    
    /**
     * Функция для добавления патерна регулярных выражений
     * 
     * @param str $key
     * @param $pattern
     */
    public function addPattern($key, $pattern) {
        $this->patterns[$key]=$pattern;
    }
    
    /**
     * Функция для проверки на наличие данного метода обращения массива routes
     * 
     * @param str $method
     * @return array
     */
    public function routes($method){
        return isset($this->routes[$method]) ? $this->routes[$method] : [];
    }
    
    /**
     * Функция для регистрации роутов
     * 
     * @param type $method
     * @param type $pattern
     * @param type $controller
     */    
    public function register($method,$pattern,$controller) {
        $convert = $this->convertPattern($pattern);
        
        $this->routes[strtoupper($method)][$convert]=$controller;
    }
    
    private function convertPattern($pattern) {
        if(strpos($pattern,'(')===false){
            return $pattern;
        }
        
        return preg_replace_callback('#\((\w+):(\w+)\)#',[$this,'replacePattern'] , $pattern);
    }
    
    private function replacePattern($matches){
        return '(?<'.$matches[1].'>'.strtr($matches[2],$this->patterns).')';
    }
    
    /**
     * Функция для очистки ненужных ключей 
     * 
     * @param array $parameters
     * @return array
     */
    private function processParam($parameters){
        foreach($parameters as $key => $value){
            if(is_int($key)){
                unset($parameters[$key]);
            }
        }
        return $parameters;
    }

    /**
     * 
     * @param string $method
     * @param type $uri
     * @return \Engine\Core\Router\DispatchedRoute
     */
    public function dispatch($method, $uri) {
        $routes = $this->routes(strtoupper($method));
        
        if(array_key_exists($uri,$routes)){
            return new DispatchedRoute($routes[$uri]);
        }
        
        return $this->doDispatch($method, $uri);
    }
    
    private function doDispatch($method, $uri) 
    {
        foreach ($this->routes(strtoupper($method)) as $route => $controller)
        {
            $pattern= '#^' . $route . '$#s';
            
            if(preg_match($pattern,$uri,$parameters))
            {
               return new DispatchedRoute($controller,$this->processParam($parameters)); 
            }
        }
    }
}
