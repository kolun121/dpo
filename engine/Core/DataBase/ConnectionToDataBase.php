<?php

namespace Engine\Core\DataBase;

use Engine\Core\Config\Config;
use PDO;

class ConnectionToDataBase {
    
    private $link;
   
    public function __construct() {
        $this->connect();
    }
    private function connect(){
        $config = Config::group('database');
        $dsn = 'mysql:host='.$config['host'].';dbname='.$config['db_name'].';charset='.$config['charset'];
        $this->link=new \PDO($dsn,$config['user'],$config['password']);
        $this->link->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    }
    
    public function execute($sql, $values = []) {
        $sth = $this->link->prepare($sql);
        $sth->execute($values);
        return $sth;
    }
    
    public function query($sql, $values = []){
        $sth = $this->link->prepare($sql);
        $sth->execute($values);
        
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
//        $sth = $this->execute($sql, $values);
//        
//	$result = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        if($result === false){
            return [];
        }
        
        return $result;
	/*while($row = $q->fetch()){
		echo "$row[name], $row[date], $row[type_of_block], $row[type_of_course], $row[image] <br>";
		
	}*/
    }
    
    public function lastInsertId(){
        return $this->link->lastInsertId();
    }
}
