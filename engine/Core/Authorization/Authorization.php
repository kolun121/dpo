<?php

namespace Engine\Core\Authorization;

use Engine\Helper\Cookie;

class Authorization implements AuthorizationInterface{
    
    protected $authorized = false;
    protected $user;
    
    public function authorized()
    {
        return $this->authorized;
    }
    
    public function user()
    {
        return COOKIE::get('auth_user');
    }
    
    /**
     * Функция для авторизации пользователя
     * 
     * @param type $user
     */
    public function authorize($user)
    {
        Cookie::set('auth_authorized' , TRUE);
        Cookie::set('auth_user' , $user);
        
        $this->authorized = true;
        $this->user = $user;
    }
    
    /**
     * Функция для выхода
     */
    public function unAuthorize()
    {
        Cookie::delete('auth_authorized');
        Cookie::delete('auth_user');
        
        $this->authorized = FALSE;
        $this->user = null;
    }
    
    //Для безопасности
    public static function salt()
    {
        return (string) rand(10000000,99999999);
    }
    
    /**
     * Создание хеша пароля
     * 
     * @param type $password
     * @param type $salt
     * @return type
     */
    public static function encryptPassword($password, $salt = '')
    {
        return hash('sha256',$password . $salt);
    }
}
