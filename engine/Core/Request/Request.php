<?php

namespace Engine\Core\Request;
class Request {
    /**
     * Хранение глобальной переменной GET
     * 
     * @var array
     */
    public $get = [];
    
    /**
     * Хранение глобальной переменной POST
     * 
     * @var array
     */
    public $post = [];
    
    /**
     * Хранение глобальной переменной REQUEST
     * 
     * @var array
     */
    public $request = [];
    
    /**
     *  Хранение глобальной переменной COOKIE
     * 
     * @var array
     */
    public $cookie = [];
    
    /**
     *  Хранение глобальной переменной FILES
     * 
     * @var array
     */
    public $files = [];
    
    /**
     * Хранение глобальной переменной SERVER
     * 
     * @var array
     */
    public $server = [];
    
    public function __construct() {
        $this->get     = $_GET;
        $this->post    = $_POST;
        $this->request = $_REQUEST;
        $this->cookie  = $_COOKIE;
        $this->files   = $_FILES;
        $this->server  = $_SERVER;
    }
}
