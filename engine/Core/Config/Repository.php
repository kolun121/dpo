<?php

namespace Engine\Core\Config;

class Repository
{
    /**
     * @var array Переменная для хранения конфигов
     */
    protected static $stored = [];

    /**
     * Функция для сохранения конфига
     *
     * @param  string  $group 
     * @param  string  $key   
     * @param  mixed   $data 
     * @return void
     */
    public static function store($group, $key, $data)
    {
        // Проверяем валидность массива
        if (!isset(static::$stored[$group]) || !is_array(static::$stored[$group])) {
            static::$stored[$group] = [];
        }

        // Сохраняем конфиг
        static::$stored[$group][$key] = $data;
    }

    /**
     * Функция для получения значения отдельного ключа конфига
     *
     * @param  string  $group  
     * @param  string  $key  
     * @return mixed
     */
    public static function retrieve($group, $key)
    {
        return isset(static::$stored[$group][$key]) ? static::$stored[$group][$key] : false;
    }

    /**
     * Функция для получения файла конфига
     *
     * @param  string  $group  
     * @return mixed
     */
    public static function retrieveGroup($group)
    {
        return isset(static::$stored[$group]) ? static::$stored[$group] : false;
    }
}