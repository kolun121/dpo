<?php

namespace Engine\Core\Config;

class Config {
    /**
     * Функция для получения значения отдельного ключа конфига
     * 
     * @param type $key
     * @param type $group
     * @return type
     */
    public static function item($key,$group = 'main'){
        if (!Repository::retrieve($group, $key)) {
            self::file($group);
        }

        return Repository::retrieve($group, $key);
    }
    
    /**
     * Функция для получения значения файла конфига
     *
     * @param  string  $group  The item group.
     * @return mixed
     */
    public static function group($group)
    {
        if (!Repository::retrieveGroup($group)) {
            self::file($group);
        }

        return Repository::retrieveGroup($group);
    }
    
    /**
     * Функция для получения файла конфига
     * 
     * @param type $group
     * @return boolean
     * @throws \InvalidArgumentException
     */
    public static function file($group)
    {
        //Путь к конфигу
        $path = path('config') . DS . $group . '.php';
        
        if(file_exists($path))
        {
            $items = include $path;
            
            if (is_array($items)) {
                // Сохраняем
                foreach ($items as $key => $value) {
                    Repository::store($group, $key, $value);
                }

                // Успешная загрузка файла
                return true;
            }
            else
            {
                throw new \InvalidArgumentException(
                        sprintf('Файл конфигурации "%s" не валиден',$path)
                        );
            }
        }
        else
        {
            throw new \InvalidArgumentException(sprintf(
                    'Файл конфигурации "%s" не найден', $path
                    ));
        }
        return false;
    }
}
