<?php

namespace Engine\Service;

abstract class AbstractProvider {
    /**
     *
     * @var \Engine\DI\DI; 
     */
    protected $di;
    
    /**
     * Абстрактный контруктор
     * 
     * @param \Engine\DI\DI $di
     */
    public function __construct(\Engine\DI\DI $di) {
        $this->di=$di;
    }
    
    /**
     * Инициализация сервиса
     * 
     */
    abstract function init();
}
