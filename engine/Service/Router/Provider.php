<?php

namespace Engine\Service\Router;

use Engine\Service\AbstractProvider;
use Engine\Core\Router\Router;

class Provider extends AbstractProvider {
    /**
     * Имя сервиса
     * 
     * @var String
     */
    public $serviceName='router';
    
   /**
    * 
    * return mixed
    */
    public function init() 
    {
        $router = new Router('http://project3.std-236.ist.mospolytech.ru/');
        
        $this->di->set($this->serviceName,$router);
    }
}
