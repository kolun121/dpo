<?php

namespace Engine\Service\DataBase;

use Engine\Service\AbstractProvider;
use Engine\Core\DataBase\ConnectionToDataBase;
class Provider extends AbstractProvider {
    /**
     * Имя сервиса
     * 
     * @var String
     */
    public $serviceName='db';
    
   /**
    * 
    * return mixed
    */
    public function init() {
    
        $db = new ConnectionToDataBase();
        $this->di->set($this->serviceName,$db);
    }
}
