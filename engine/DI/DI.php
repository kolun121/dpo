<?php

namespace Engine\DI;

class DI {
    /**
     *
     * @var array Массив для хранения зависимостей 
     */
    private $container = [];
    
    /**
     * Функция для добавления зависимости в массив container
     * 
     * @param $key String Ключ зависимости
     * @param $value Object Значение зависимости
     * @return $this
     */
    public function set($key,$value) {
        $this->container[$key] = $value;
        return $this;
    }
    
    /**
     * Функция для получения зависимоти по ключю
     * 
     * @param $key
     * @return Object
     */
    function get($key) {
        return $this->has($key);
    }
    
    /**
     * Функция для проверки существования значений в массиве container
     * 
     * @param $key
     * @return boolean
     */    
    public function has($key) {
        return isset($this->container[$key]) ? $this->container[$key] : null;
    }

    public function push($key, $element = []) {
        if ($this->has($key) !== null){
            $this->set($key,[]);
        }
        
        if(!empty($element)){
            $this->container[$key] [$element['key']] = $element['value'];
        }
    }
    

}
