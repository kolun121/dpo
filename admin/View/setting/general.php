<?php $this->theme->header(); ?>

<main>
    <div class="container">
        <div class="row">
            <div class="col page-title">
                <h3>Настройки</h3>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <form>
                    <?php foreach($settings as $setting):?>
                    <div class="form-group row">
                        <label for="formNameSite" class="col-2 col-form-label">
                            <?= $setting->name ?>
                        </label>
                        <div class="col-10">
                            <input class="form-control" type="text" name="<?= $setting->get_field ?>" value="<?= $setting->value ?>" id="formNameSite">
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
</main>

<?php $this->theme->footer(); ?>