<?php $this->theme->header()?>
    
<main>
    <div class="container">
        <div class="row">
            <div class="col-9">
                <h1>Создание курса</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-9">
                <form>
                    <div class="form-group">
                      <label for="formTitle">Заголовок</label>
                      <input type="text" class="form-control" id="formTitle" placeholder="Заголовок страницы">
                    </div>
                    <div class="form-group">
                        <label for="formContent">Текст</label>
                        <textarea class="form-control" id="formContent"></textarea>
                    </div>
                 </form>
            </div>
            <div class="col-3">
                <h3>Добавить</h3>
                <button type="submit" class="btn btn-primary" onclick="course.add()">
                    Добавить
                </button>
            </div>
        </div>
    </div>
</main>

<?php $this->theme->footer()?>

