<?php $this->theme->header()?>
    
<main>
    <div class="container">
        <h1>Страница <a href="/admin/courses/create/">Создать курс</a></h1>
    </div>
    
    <table class="table">
        <thead>
        <tr>
            <td>ID</td>
            <td>Заголовок</td>
            <td>Текст</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($courses as $course): ?>
            <tr>
                <th><?= $course['id'] ?></th>
                <td>
                    <a href="/admin/courses/edit/<?= $course['id'] ?>">
                         <?= $course['title'] ?>
                    </a>
                </td>
                <td><?= $course['content'] ?></td>
                <td><?= $course['date'] ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</main>

<?php $this->theme->footer()?>