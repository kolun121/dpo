<?php $this->theme->header()?>
    
<main>
    <div class="container">
        <h1>Страница <a href="/admin/teachers/create/">Добавить учителя</a></h1>
    </div>
    
    <table class="table">
        <thead>
        <tr>
            <td>ID</td>
            <td>Заголовок</td>
            <td>Текст</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($teachers as $teacher): ?>
            <tr>
                <th><?= $teacher['id'] ?></th>
                <td>
                    <a href="/admin/teachers/edit/<?= $teacher['id'] ?>">
                         <?= $teacher['title'] ?>
                    </a>
                </td>
                <td><?= $teacher['content'] ?></td>
                <td><?= $teacher['date'] ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</main>

<?php $this->theme->footer()?>