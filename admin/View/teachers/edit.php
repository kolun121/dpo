<?php $this->theme->header()?>
    
<main>
    <div class="container">
        <div class="row">
            <div class="col-9">
                <h2><?= $teacher['title'] ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-9">
                <form>
                    <div class="form-group">
                      <input type="hidden" name="teacher_id" id="formCourseId" value="<?= $teacher['id']?>" />
                      <label for="formTitle">Заголовок</label>
                      <input type="text" class="form-control" id="formTitle" name="title"
                             placeholder="Заголовок страницы" value="<?= $teacher['title']?>">
                    </div>
                    <div class="form-group">
                        <label for="formContent">Текст</label>
                        <textarea class="form-control" id="formContent" name="content"
                                  ><?= $teacher['content']?></textarea>
                    </div>
                 </form>
            </div>
            <div class="col-3">
                <h3>Обновить</h3>
                <button type="submit" class="btn btn-primary" onclick="teacher.update()">
                    Обновить
                </button>
            </div>
        </div>
    </div>
</main>

<?php $this->theme->footer()?>

