<?php

/* 
 * Список путей
 */

$this->router->add('login','/admin/login/','LoginController:form');
$this->router->add('logout','/admin/logout/','AdminController:logout');
$this->router->add('auth-admin','/admin/auth/','LoginController:authAdmin','POST');
$this->router->add('dashboard','/admin/','DashboardController:index');
//Создание курса
//GET запросы
$this->router->add('courses','/admin/courses/','CoursesController:listing');
$this->router->add('course-create','/admin/courses/create/','CoursesController:create');
$this->router->add('course-edit','/admin/courses/edit/(id:int)','CoursesController:edit');
//POST запросы
$this->router->add('course-add','/admin/courses/add/','CoursesController:add', 'POST');
$this->router->add('course-update','/admin/courses/update/','CoursesController:update', 'POST');

//Учителя
//GET запросы
$this->router->add('teachers','/admin/teachers/','TeachersController:listing');
$this->router->add('teacher-create','/admin/teachers/create/','TeachersController:create');
$this->router->add('teacher-edit','/admin/teachers/edit/(id:int)','TeachersController:edit');
//POST запросы
$this->router->add('teacher-add','/admin/teachers/add/','TeachersController:add', 'POST');
$this->router->add('teacher-update','/admin/teachers/update/','TeachersController:update', 'POST');

//Настройки
//GET запросы
$this->router->add('settings-general','/admin/settings/general/','SettingController:general');