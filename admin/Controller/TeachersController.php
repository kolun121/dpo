<?php

namespace Admin\Controller;

class TeachersController extends AdminController{
    public function listing(){
        $this->load->model('Teacher');
        $this->data['teachers'] = $this->di->get('model')->teacher->getTeachers();
        $this->view->render('teachers/list', $this->data);
    }
    
    public function create(){
        $this->load->model('Teacher');
                
        //Берем шаблон
        $this->view->render('teachers/create');
    }
    
    public function edit($id){
        $this->load->model('Teacher');        
        $this->data['teacher'] = $this->di->get('model')->teacher->getTeacherData($id);
        $this->view->render('teachers/edit', $this->data);
    }
    
    public function add(){
        $this->load->model('Teacher');
        $params = $this->request->post;
        //Массив для последущей проверки передаваемых параметров
       
        if(isset($params['title'])){
            $teacherId = $this->di->get('model')->teacher->createTeacher($params);            
            echo $teacherId;
        }
    }
    
    public function update(){
        $params = $this->request->post;
        $this->load->model('Teacher');

        //Массив для последущей проверки передаваемых параметров
        //$isCourseParams = ['title','content'];
       
        if(isset($params['title'])){
            $teacherId = $this->di->get('model')->teacher->updateTeacher($params);            
            echo $teacherId;
        }
    }
}
