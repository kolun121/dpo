<?php

namespace Admin\Controller;

class CoursesController extends AdminController{
    public function listing(){
        $this->load->model('Course');
        $this->data['courses'] = $this->di->get('model')->course->getCourses();
        $this->view->render('pages/list', $this->data);
    }
    
    public function create(){
        $this->load->model('Course');
                
        //Берем шаблон
        $this->view->render('pages/create');
    }
    
    public function edit($id){
        $this->load->model('Course');
        
        $this->data['course'] = $this->di->get('model')->course->getCourseData($id);
        $this->view->render('pages/edit', $this->data);
    }
    
    public function add(){
        $this->load->model('Course');
        $params = $this->request->post;
        //Массив для последущей проверки передаваемых параметров
        //$isCourseParams = ['title','content'];
       
        if(isset($params['title'])){
            $courseId = $this->di->get('model')->course->createCourse($params);            
            echo $courseId;
        }
    }
    
    public function update(){
        $params = $this->request->post;
        $this->load->model('Course');

        //Массив для последущей проверки передаваемых параметров
        //$isCourseParams = ['title','content'];
       
        if(isset($params['title'])){
            $courseId = $this->di->get('model')->course->updateCourse($params);            
            echo $courseId;
        }
    }
}
