<?php

namespace Admin\Controller;

use Engine\Controller;
use Engine\Core\Authorization\Authorization;
use Engine\Core\DataBase\QueryBuilder;

class LoginController extends Controller {
    
    protected $auth;
    public function __construct(\Engine\DI\DI $di) {
        parent::__construct($di);
        
        $this->auth = new Authorization();
        
        if($this->auth->user() !== null){
            header('Location: /admin/');
            exit;
        }
        
//        if($this->auth->authorized()){
//            header('Location: /admin/',true,301);
//            exit;
//        }
        
    }
    
    public function form() {
        $this->di->get('view')->render('login');
        //$this->view->render('login');
    }
    
    public function authAdmin(){
        $queryBuilder = new QueryBuilder();
        
        $params = $this->request->post;
        
        $sql = $queryBuilder
                ->select()
                ->from('user')
                ->where('email',$params['email'])
                ->where('password', $params['password'])
                ->limit(1)
                ->sql();
        
        $query = $this->db->query($sql, $queryBuilder->values);
        if(!empty($query)){
            $user = $query[0];
            
            if($user['role'] == 'admin'){
                
                $this->auth->authorize($user['role']);
                header('Location: /admin/');
                exit;
            }
        }
        echo 'Неверное имя или пароль';
    }
}
