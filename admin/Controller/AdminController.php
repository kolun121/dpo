<?php

namespace Admin\Controller;

use Engine\Controller;
use Engine\Core\Authorization\Authorization;

class AdminController extends Controller{
    protected $auth;
    
    public $data = [];
    /**
     * Конструкор AdminController
     * @param \Engine\DI\DI $di
     */
    public function __construct(\Engine\DI\DI $di) {
        parent::__construct($di);
        
        $this->auth = new Authorization();
        
        if($this->auth->user() == null){
            header('Location: /admin/login/');
            exit;
        }
    }
    
    /**
     * Функция проверки авторизации
     */
    public function checkAutorization() {
        
        if($this->auth->user() != null){
            $this->auth->authorize($this->auth->user());
        }
   
        if (!$this->auth->authorized())
        {
            //Перенапраляем
            header('Location: /admin/login/');
            exit;
        }
    }
    
    public function logout() {
        $this->auth->unAuthorize();
        header('Location: /admin/login/');
        exit;
    }
}
