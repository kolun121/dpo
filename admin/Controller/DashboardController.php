<?php

//Контроллер в который мы попадаем при входе в админку

namespace Admin\Controller;

class DashboardController extends AdminController {
    public function index(){
        
        $userModel = $this->load->model('User');
                
        $this->view->render('dashboard');
        //$this->view->render('login');
    }
}
