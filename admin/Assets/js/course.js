var course = {
    ajaxMethod: 'POST',

    add: function() {
        var formData = new FormData();

        formData.append('title', $('#formTitle').val());
        formData.append('content', $('#formContent').val());

        $.ajax({
            url: '/admin/courses/add/',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                console.log(result);
                window.location = '/admin/courses/edit/' + result;
            }
        });
    },
    update: function() {
        var formData = new FormData();

        formData.append('course_id', $('#formCourseId').val());
        formData.append('title', $('#formTitle').val());
        formData.append('content', $('#formContent').val());

        $.ajax({
            url: '/admin/courses/update/',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                console.log(result);
            }
        });
    }
};

console.log(course);