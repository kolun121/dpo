var teacher = {
    ajaxMethod: 'POST',

    add: function() {
        var formData = new FormData();

        formData.append('title', $('#formTitle').val());
        formData.append('content', $('#formContent').val());

        $.ajax({
            url: '/admin/teachers/add/',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                console.log(result);
                window.location = '/admin/teachers/edit/' + result;
            }
        });
    },
    update: function() {
        var formData = new FormData();

        formData.append('teacher_id', $('#formCourseId').val());
        formData.append('title', $('#formTitle').val());
        formData.append('content', $('#formContent').val());

        $.ajax({
            url: '/admin/teachers/update/',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                console.log(result);
            }
        });
    }
};