<?php
namespace Admin\Model\Teacher;

use Engine\Model;

class TeacherRepository extends Model{
    public function getTeachers(){
        $sql = $this->queryBuilder->select()
                ->from('teachers')
                ->orderBy('id', 'ASC')
                ->sql();
        
        return $this->db->query($sql);
    }
    
    public function getTeacherData($id) {
        $teacher = new Teacher($id);
        
        return $teacher->findOne();
    }
    
    public function createTeacher($params) {

        $teacher = new Teacher;
        $teacher->setTitle($params['title']);
        $teacher->setContent($params['content']);
        $teacherId = $teacher->save();
        
        return $teacherId;
    }
    
    public function updateTeacher($params) {

        if (isset($params['teacher_id'])){
            $teacher = new Teacher($params['teacher_id']);
            $teacher->setTitle($params['title']);
            $teacher->setContent($params['content']);
            $teacher->save(); 
        }

    }
}

