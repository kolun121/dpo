<?php
namespace Admin\Model\Course;

use Engine\Model;

class CourseRepository extends Model{
    public function getCourses(){
        $sql = $this->queryBuilder->select()
                ->from('courses')
                ->orderBy('id', 'ASC')
                ->sql();
        
        return $this->db->query($sql);
    }
    
    public function getCourseData($id) {
        $course = new Course($id);
        
        return $course->findOne();
    }
    
    public function createCourse($params) {

        $course = new Course;
        $course->setTitle($params['title']);
        $course->setContent($params['content']);
        $courseId = $course->save();
        
        return $courseId;
    }
    
    public function updateCourse($params) {

        if (isset($params['course_id'])){
            $course = new Course($params['course_id']);
            $course->setTitle($params['title']);
            $course->setContent($params['content']);
            $course->save(); 
        }

    }
}

