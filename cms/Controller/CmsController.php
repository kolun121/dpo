<?php

namespace Cms\Controller;

class CmsController extends \Engine\Controller{
    /**
     * Конструкор CmsController
     * @param \Engine\DI\DI $di
     */
    public function __construct(\Engine\DI\DI $di) {
        parent::__construct($di);
    }
}
