<?php

namespace Cms\Model\Menu;

use Engine\Core\DataBase\ActiveRecord;

class Menu
{
    use ActiveRecord;
    
    protected $table = 'menu';
    
    public $id;
    
    public $name;
    
    public $parent;
    
    public $position;
    
    public $link;
    
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getParent() {
        return $this->parent;
    }

    function getPosition() {
        return $this->position;
    }

    function getLink() {
        return $this->link;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setParent($parent) {
        $this->parent = $parent;
    }

    function setPosition($position) {
        $this->position = $position;
    }

    function setLink($link) {
        $this->link = $link;
    }

}
